### 1. Deploy the initial code
 * Enter the `image-api-node` folder
 * Install js packages: `> npm install`
 * Run and verify the local unit tests: `> npm test`
 * Create or edit ~/.aws/credentials and add the entry:
```
   [serverless-workshop-agent]
   aws_access_key_id     = provided_key
   aws_secret_access_key = provided_secret
   region                = eu-west-1
```
   These are the credentials that the Serverless framework will use when talking to aws to
   create the resources we define.  
   
 * Have a look around the `serverless.yml`file. NOTE that you must fill in a "username".
 * Then run `> sls deploy`.   
   This will package and deploy the current state of the repo.  
 * Look at the output, it will show you http endpoints for the functions 
   defined in serverless.yml. Make a POST request against the `/images` endpoint
   with a `{url: url} json object as payload, for example with curl:    
   ```curl -X POST https://abc.execute-api.eu-west-1.amazonaws.com/dev/images/ -d '{"url":"https://www.stickpng.com/assets/images/580b57fcd9996e24bc43c4e7.png"}'```  
   You should get an error, lets check the logs!
 * Find your function in the [aws Lambda console](https://eu-west-1.console.aws.amazon.com/lambda/home)
   (make sure you've got the right region selected), and check the cloudwatch logs.   
   You should see something about that the s3 bucket does not exist. 
   The code is making a request to save the image to s3, but we haven't created any s3 bucket yet.   
  

### 2. Define and deploy the s3 bucket
 - We can declare any other resources our code needs in the same serverless.yml file 
   where we've defined the functions, such as databases and file storage. For AWS, this
   is done by including a CloudFormation section (Amazon's "infrastracture as code" language).  
   When deploying with ```sls deploy``` Serverless will create these resources for us.  
   Add the following to you serverless.yml:
```
resources:
 Resources:
   S3Images:
     Type: AWS::S3::Bucket 
     Properties:
       BucketName: ${self:custom.s3_images_bucket}

```
   Note how it references the "custom.s3_images_bucket" property defined earlier, and also
   that we are exposing this bucket name as an environment variable available to the functions.  
   Run `sls deploy` again.   
   If you go to the [s3 console](https://eu-west-1.console.aws.amazon.com/lambda/home)
   you should see the bucket has been created.
   Try again to make a request to add an image as above. This time you'll see in the logs 
   we're getting an "Access denied" error when writing to s3.  

### 3. Give the lambda permission to access the s3 bucket
On amazon access to resources is managed using IAM users and profiles.
The functions execute using a profile created by the Serverless framework, and by
default this role is not allowed to access resources such as s3.   
To grant the lambda function access to s3, add the following as a property under the "provider" section in
serverless.yml:
```
  iamRoleStatements:
    - Effect: "Allow"
      Action:
        - s3:PutObject
        - s3:DeleteObject
        - s3:GetObject
      Resource: "arn:aws:s3:::${self:custom.s3_images_bucket}/*"
```
See an example [here](https://serverless.com/framework/docs/providers/aws/guide/functions#permissions)   
Deploy, and make the same request again to add an image.
You should get a 200 response with an image id:
```javascript
{"imageId":"iFfqfenDXNsq485qkNoceB"}
```
If you go to the [s3 console](https://s3.console.aws.amazon.com/s3/home?region=eu-west-1), you'll see the image has been stored there with the given id.  
You can now GET info about this image:  
`curl https://abc.execute-api.eu-west-1.amazonaws.com/dev/images/iFfqfenDXNsq485qkNoceB`  
Currently this will only give you back the url you previously posted, so lets improve that. 

### 4. Add a function for getting image tags
In image_tagger.js there is code that given an image stored in s3 will 
use the AWS Rekognition service to automatically get "tags" for the image,
e.g., "cats" if there are cats in the image.   
We will add a new function, which unlike the other two handlers will not be exposed over HTTP but trigger
when an image is stored in s3:  
  * The image tagging code uses the AWS Rekognition service, and as we did for s3 
  we must grant access to Rekognition. Add another entry under `iamRoleStatements`:
``` 
    - Effect: "Allow"
      Action:
        - "rekognition:*"
      Resource: "*"
``` 
  * Add a new function definition `tag_image` under `functions` in serverless.yml.   
   You want to trigger the
   handler in src/tag_image_handler when a file with suffix "_image" is written
   to s3.   
   See an example [here](https://serverless.com/framework/docs/providers/aws/events/s3#using-existing-buckets);
   adapt it by filling in the right handler, s3 bucket, and the "_image" suffix. 
  * Fill in the missing pieces in tag_image_handler.js 
  
If you now POST a new url, you should see the image tagger function has been triggered, 
and when GETing the image id you should hopefully get some tags back :)
