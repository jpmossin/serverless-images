import requests
import threading
import time
import numpy as np

url = ""
max_concurrent = 15

resp_times = []
done = False


def make_requests():
    print("Starting request loop")
    while not done:
        resp = requests.get(url)
        if resp.status_code != 200:
            print("Unexpected status code: %d" % resp.status_code)
        resp_times.append(resp.elapsed.total_seconds())


for _ in range(max_concurrent):
    thread = threading.Thread(target=make_requests)
    thread.start()
    time.sleep(1)

done = True
time.sleep(1)
print("total requests: ", len(resp_times))
sorted_responses = np.sort(resp_times)
print("worst: ", sorted_responses[-5:])
print("best:", sorted_responses[:5])

# expected: median and 90-p is good, but ~max_concurrent (a bit less) slow requests
print("percentiles: ", np.percentile(resp_times, [50, 90, 99]))
