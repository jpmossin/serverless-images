const AWS = require('aws-sdk');
const ImageTagger = require("./image_tagger");
const {createImageClient} = require("./handler_utils");

const rek = new AWS.Rekognition();
const s3 = new AWS.S3();
const imageClient = createImageClient();
const tagger = new ImageTagger(rek, s3, imageClient);

module.exports.handler = async function (event) {
  console.log("tagger event:", JSON.stringify(event));
  // todo: the event will have info about which s3 object was created.
  // You can run this function as it is (sls deploy, then post an image) and view the
  // above log output to see the structure of the event object when an image
  // is saved in s3.

  // Retrieve the bucket and key and do:
  // const bucket = {bucket_name}
  // const key = {key_name}
  // await tagger.tagImage(bucket, key);
};
