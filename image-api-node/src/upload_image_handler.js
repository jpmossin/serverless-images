'use strict';

const {ok200, badRequest, createImageClient} = require("./handler_utils");
const imageClient = createImageClient();

module.exports.handler = async function (event, _ctx) {
  const url = extractImageUrl(event.body);
  if (!url) {
    return badRequest("Please provide a payload with an image url")
  }

  const {imageId, clientError} = await imageClient.fetchAndSaveImage(url);
  if (clientError) {
    return badRequest(clientError);
  }
  return ok200({imageId})
};

function extractImageUrl(eventBody) {
  try {
    return (JSON.parse(eventBody) || {}).url;
  } catch (e) {
    console.log("Failed parsing event", JSON.stringify(e));
    return null;
  }
}
