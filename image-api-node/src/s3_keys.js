function getInfoFileKey (imageId) {
  return imageId + "_info.json";
}

function getImageFileKey(imageId) {
  return imageId + "_image";
}

function imageKeyToInfoKey(imageKey) {
  return getInfoFileKey(imageKey.split("_image")[0]);
}

module.exports = {
  getInfoFileKey,
  getImageFileKey,
  imageKeyToInfoKey,
};
