const ImageClient = require('./image_client');
const AWS = require('aws-sdk');
const fetch = require('node-fetch');
const shortUuid = require('short-uuid');

function createImageClient() {
  const s3Bucket = process.env.S3_IMAGES_BUCKET || 'serverless-workshop-images-localdev';
  const s3 = new AWS.S3();
  const uuid = () => shortUuid.generate();
  return new ImageClient(s3, s3Bucket, fetch, uuid);
}

function ok200(respObj) {
  return {
    statusCode: 200,
    body: JSON.stringify(respObj),
  };
}

function badRequest(msg) {
  console.log("bad request: " + msg);
  return {
    statusCode: 400,
    body: JSON.stringify({clientError: msg})
  }
}

module.exports = {
  ok200,
  badRequest,
  createImageClient,
};
