const {imageKeyToInfoKey} = require("./s3_keys");

/**
 * Uses aws Rekognition to get tags for an image
 */
function ImageTagger(rekognition, s3, imageClient) {
  this.rek = rekognition;
  this.s3 = s3;
  this.imageClient = imageClient;
}

ImageTagger.prototype.tagImage = async function (bucket, key) {
  const tags = await this._getTags(bucket, key);
  console.log("got tags: " + tags);
  const infoFileKey = imageKeyToInfoKey(key);
  await this._updateInfoFile(bucket, infoFileKey, tags);
};

ImageTagger.prototype._getTags = async function (bucket, key) {
  const params = {
    Image: {
      S3Object: {
        Bucket: bucket,
        Name: key
      }
    },
    MaxLabels: 10,
    MinConfidence: 70
  };
  console.log("getting tags for:", JSON.stringify(params));
  const labelResp = await this.rek.detectLabels(params).promise();
  return labelResp.Labels.map(label => label.Name);
};

ImageTagger.prototype._updateInfoFile = async function (bucket, imageKey, tags) {
  const info = await this.imageClient.getImageInfo(imageKey);
  info.tags = tags;
  await this.imageClient.writeImageInfoFile(imageKey, info)
};

module.exports = ImageTagger;
