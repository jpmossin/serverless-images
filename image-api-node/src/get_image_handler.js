const {ok200, createImageClient} = require("./handler_utils");
const {getInfoFileKey} = require("./s3_keys");
const imageClient = createImageClient();

module.exports.handler = async function (event, _ctx) {
  const imageId = event.pathParameters.image_id;
  const key = getInfoFileKey(imageId);
  const imageInfo = await imageClient.getImageInfo(key);
  return ok200(imageInfo);
};
