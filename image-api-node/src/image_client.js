const path = require("path");
const {getInfoFileKey, getImageFileKey} = require("./s3_keys");

function ImageClient(s3, s3Bucket, fetch, uuid) {
  this.s3 = s3;
  this.s3Bucket = s3Bucket;
  this.fetch = fetch;
  this.uuid = uuid;
  this.acceptedImageTypes = new Set(["jpg", "png"]);
}

/**
 * Fetch the info file for the given image
 */
ImageClient.prototype.getImageInfo = async function (key) {
  const params = {Bucket: this.s3Bucket, Key: key};
  try {
    const resp = await this.s3.getObject(params).promise();
    return JSON.parse(resp.Body.toString())
  } catch (e) {
    console.log("error fetching image: " + key);
    console.log(JSON.stringify(e));
    throw e;
  }
};

/**
 * Fetch an image and store it in s3 together with a
 * default info file for the image
 */
ImageClient.prototype.fetchAndSaveImage = async function (url) {
  const extension = path.extname(url).substr(1);
  if (!this.acceptedImageTypes.has(extension)) {
    return {clientError: "Image type not accepted: " + extension};
  }
  const {body, clientError} = await this._fetchImage(url);
  if (clientError) {
    return {clientError};
  }

  const imageId = this.uuid();
  const infoKey = getInfoFileKey(imageId);
  const imageKey = getImageFileKey(imageId);
  const imageUploadPromise = this._uploadImage(imageKey, body);
  const imageInfoPromise = this.writeImageInfoFile(infoKey, {url: url});
  try {
    await Promise.all([imageUploadPromise, imageInfoPromise]);
  } catch (e) {
    console.log("failed saving image", JSON.stringify(e));
    throw e;
  }
  console.log(`done, imageId: ${imageId}`);
  return {imageId};
};

ImageClient.prototype.writeImageInfoFile = function (key, infoObj) {
  const params = {Bucket: this.s3Bucket, Key: key, Body: JSON.stringify(infoObj)};
  console.log(`Writing image info file, key: ${key}`);
  return this.s3.upload(params).promise();
};


ImageClient.prototype._fetchImage = async function (url) {
  console.log(`starting fetch of url: ${url}`);
  const resp = await this.fetch(url);
  const contentType = resp.headers.get('content-type');
  if (!contentType.startsWith("image/")) {
    return {clientError: "Content type not supported: " + contentType};
  }
  return {body: resp.body};
};


ImageClient.prototype._uploadImage = async function (key, imgBodyStream) {
  const params = {Bucket: this.s3Bucket, Key: key, Body: imgBodyStream};
  console.log(`starting image upload, key: ${key}`);
  return this.s3.upload(params).promise();
};


module.exports = ImageClient;
