const assert = require('assert');
const {describe, beforeEach, it} = require("mocha");
const ImageClient = require("../src/image_client");
const fetch = require("node-fetch");
const AWS = require('aws-sdk');
const AWSMock = require('aws-sdk-mock');

describe('ImageClient', function () {

  const s3Bucket = "dummy-bucket";
  const pngUrl = "https://www.google.co.uk/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png";
  const gifUrl = "https://en.wikipedia.org/wiki/GIF#/media/File:Rotating_earth_(large).gif";
  const dummyUuid = () => "123";
  const expectedImageFile = dummyUuid() + "_image";
  const expectedInfoFile = dummyUuid() + "_info.json";

  describe('fetchAndSaveImage', function () {

    let s3UploadedFiles;
    beforeEach(function () {
      s3UploadedFiles = new Set();
    });

    AWSMock.mock('S3', 'upload', function (params, callback) {
      s3UploadedFiles.add(params.Key);
      return callback(null, {Bucket: params.Bucket, Key: params.Key});
    });
    const s3 = new AWS.S3();
    const imageClient = new ImageClient(s3, s3Bucket, fetch, dummyUuid);

    it('should fetch the given image and store it in s3', async function () {
      const res = await imageClient.fetchAndSaveImage(pngUrl);
      assert.strictEqual(res.imageId, "123");
      assert.strictEqual(s3UploadedFiles.size, 2);
      assert(s3UploadedFiles.has(expectedInfoFile));
      assert(s3UploadedFiles.has(expectedImageFile));
    });

    it('should reject resources that are not images', async function () {
      const res = await imageClient.fetchAndSaveImage(gifUrl);
      assert(res.clientError)
    });
  });

  describe("getImageInfo", function () {
    const dummyInfo = JSON.stringify({url: "foo", tags: ["cats"]});
    AWSMock.mock('S3', 'getObject', function (params, callback) {
      if (params.Key === expectedInfoFile) {
        callback(null, {Bucket: params.Bucket, Key: params.Key, Body: Buffer.from(dummyInfo)});
      } else {
        callback(new Error("Unknown key: " + params.Key))
      }
    });
    const s3 = new AWS.S3();
    const imageClient = new ImageClient(s3, s3Bucket, fetch, dummyUuid);

    it("should return the stored info for the image", async function () {
      const res = await imageClient.getImageInfo(expectedInfoFile);
      assert.strictEqual(JSON.stringify(res), dummyInfo);
    });

    it("should throw for an invalid image id", async function () {
      let error;
      try {
        await imageClient.getImageInfo("123invalid");
      } catch (e) {
        error = e;
      }
      assert(error);
    })
  });
});
